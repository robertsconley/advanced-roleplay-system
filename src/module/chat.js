import * as debug from './debug.js';
import * as actionManager from './apps/action.js';
import { ARSDice } from './dice/dice.js';
import { PlaceCastShape } from './castshape/castshape.js';
import * as effectManager from './effect/effects.js';
import * as utilitiesManager from './utilities.js';
import { ARSRollAttack, ARSRollDamage } from './dice/rolls.js';

/**
 *
 * Hide/show buttons based on user type (isGM/etc)
 *
 * @param {*} message
 * @param {*} html
 * @param {*} data
 */
export function renderChatMessages(message, html, data) {
    // console.log("chat.js", "-------------------renderChatMessages-----------------------", { message, html, data });

    // hide buttons for non-owners of the chatCard
    updateForOwnerButtons(message, html);

    // console.log("", { message }, message.isContentVisible, message.isAuthor, game.user.isGM)
    // check for identified npcs and hide/show names for such
    updateForNPCIdentification(message, html);

    updateForItemIdentification(message, html);

    // hide gm-view-only class chat fields from non-gm
    updateForGMOnlyViews(message, html);

    // hide owner-view-only class from non-owners
    updateForOwnerOnlyViews(message, html);

    // hide blind rolls from chat if not owner or GM
    updateForBlindRolls(message, html);

    // hide secure values
    updateForSecureValues(message, html);

    //TODO add data.sound to specials for this to trigger
    // if (message.data.sound)
    //     AudioHelper.play({ src: message.data.sound });
}

/**
 * Update visibility of buttons within a chat card based on user roles.
 * @param {Object} message - The chat message object.
 * @param {Object} html - The jQuery HTML object of the chat card.
 */
function updateForOwnerButtons(message, html) {
    const chatCard = html.find('.card-buttons');
    if (!chatCard.length) return;

    // Fetch the actor associated with the message
    const actor = game.actors.get(message.speaker.actor);
    const isOwner = Boolean(actor && actor.isOwner);
    const isGM = game.user.isGM;

    // Precompute common condition
    const hideForNonAuthorizedUsers = !isGM && !isOwner;

    // Update visibility of buttons
    updateButtonVisibility(chatCard, hideForNonAuthorizedUsers);

    // Update visibility of divs within the card
    updateDivVisibility(chatCard, hideForNonAuthorizedUsers);
}

/**
 * Update visibility of buttons within a chat card.
 * @param {Object} chatCard - The jQuery object representing the chat card.
 * @param {boolean} condition - The condition for hiding elements.
 */
function updateButtonVisibility(chatCard, condition) {
    const isGM = game.user.isGM;
    const buttons = chatCard.find('button[data-action]');
    buttons.each((_, btn) => {
        const buttonAction = btn.dataset.action || '';
        switch (buttonAction) {
            case 'undo-action-effect':
            case 'undo-healthadjust':
                if (!isGM) btn.style.display = 'none';
                break;
            default:
                if (condition) btn.style.display = 'none';
                break;
        }
    });
}

/**
 * Update visibility of divs within a chat card.
 * @param {Object} chatCard - The jQuery object representing the chat card.
 * @param {boolean} condition - The condition for hiding elements.
 */
function updateDivVisibility(chatCard, condition) {
    const divs = chatCard.find('div.card-buttons');
    divs.each((_, dv) => {
        if (condition) dv.style.display = 'none';
    });
}

/**
 * Hides class "owner-only-view" from non-owners
 * Shows class "non-owner-only-view" to non-owners
 *
 * @param {*} message
 * @param {*} html
 */
function updateForOwnerOnlyViews(message, html) {
    const actor = game.actors.get(message.speaker.actor);
    const isOwner = actor && actor.isOwner;

    // console.log("chat.js updateForOwnerOnlyViews", { message, actor, isOwner })

    // owner-only-view
    html.find('.owner-only-view').each((i, onlyOwner) => {
        if (!game.user.isGM && !isOwner) onlyOwner.setAttribute('style', 'display:none;');
    });
    // alternatively if non-owner-only-view and owner, hide that and let non-owners see it
    html.find('.non-owner-only-view').each((i, onlyOwner) => {
        if (game.user.isGM || isOwner) onlyOwner.setAttribute('style', 'display:none;');
    });
}

/**
 *
 * Only show secure values to GM
 *
 * @param {*} message
 * @param {*} html
 */
function updateForSecureValues(message, html) {
    const secureValues = html.find('.secure-value');
    // console.log("updateForSecureValues", { secureValues })
    if (secureValues) {
        secureValues.each(async (i, secureValue) => {
            // console.log("updateForSecureValues", { secureValue }, secureValue.dataset);
            const value = secureValue.dataset?.value;
            // console.log("updateForSecureValues", secureValue.innerHTML);
            if (!game.user.isGM && value) {
                // if not GM we show the value in data-value=''
                secureValue.innerHTML = `${value}`;
            }
        });
    }
}

/**
 *
 * Replace secure-name class items with proper name if not identified
 *
 * @param {*} message
 * @param {*} html
 */
function updateForItemIdentification(message, html) {
    const useItemIdentification = game.ars.config.settings.identificationItem;
    const secureNames = html.find('.secure-name');
    // console.log("updateForItemIdentification", { secureNames })
    if (secureNames.length) {
        for (let i = 0; i < secureNames.length; i++) {
            const secureNameElement = secureNames[i];
            const secureType = $(secureNameElement).data('type');
            const itemUuid = message.flags?.world?.context?.itemUuid;

            if (secureType && secureType.toLowerCase() == 'item' && itemUuid && useItemIdentification) {
                const item = fromUuidSync(itemUuid);
                if (item) {
                    $(secureNameElement).text(item.name); // Updating the text of the element
                }
            }
        }
    }
}

/**
 *
 * Hide any chat entries block entries that are marked gm-only-view if not GM
 *
 * @param {*} message
 * @param {*} html
 */
function updateForGMOnlyViews(message, html) {
    // gm-only-view
    html.find('.gm-only-view').each((i, onlyGM) => {
        if (!game.user.isGM) onlyGM.setAttribute('style', 'display:none;');
    });
}
// hide blind roll chat (even tho they can't see the roll) from players to remove
// the ability to see that a npc rolled initiative
function updateForBlindRolls(message, html) {
    // console.log("chat.js updateForBlindRolls", { message, html })
    // if blind roll and not GM then hide the message
    if (!message.isContentVisible && !message.isAuthor && !game.user.isGM) {
        //chat-message message flexcol whisper blind
        html.css('display', 'none');
    }
}
/**
 *
 * show safe names to pcs or real names if identified
 *
 * show real names to DMs
 *
 * @param {*} message
 * @param {*} html
 */
export function updateForNPCIdentification(message, html) {
    const useActorIdentification = game.ars.config.settings.identificationActor;
    const sceneId = message.speaker.scene;
    const tokenId = message.speaker.token;
    const scene = sceneId ? game.scenes.get(sceneId) : null;
    // const msgSourceToken = scene ? scene.tokens.find((tk) => tk.id === tokenId) : null;
    const secureNames = html.find('.secure-name');
    // console.log("chat.js _showSecureName", { secureNames })
    if (secureNames.length) {
        for (let i = 0; i < secureNames.length; i++) {
            const secureNameElement = secureNames[i];
            const secureTokenId = $(secureNameElement).data('id');
            const token = scene ? scene.tokens.find((tk) => tk.id === secureTokenId) : null;

            if (token && useActorIdentification) {
                $(secureNameElement).text(token.name); // Updating the text of the element
            }
        }
    }
}

export function chatListeners(html) {
    // html.on('click', '.card-buttons button', this.chatAction.bind(this));
    html.on('click', '.chatCard-expand-view', _onToggleCardDescription);
    // html.find('.spellCard-description').click(event => _onToggleSpellDescription(event, html));
    html.on('click', '.card-buttons button', chatAction);
}

// toggle between long/short spell description
export function _onToggleCardDescription(event) {
    const element = event.currentTarget;
    // toggle FA icon
    $(element).find('.fas').toggleClass('fa-compress-alt fa-expand-alt');

    const desc = $(element).parents('.chatCard-description');
    // desc.find(".chatCard-expand-view").toggleClass('fa-compress-alt fa-expand-alt');
    const shortDesc = desc.find('.chatCard-description-short');
    const fullDesc = desc.find('.chatCard-description-full');
    shortDesc.toggle();
    fullDesc.toggle();
}

/**
 *
 * This is the general function used when buttons are clicked in chat. Lots of various
 * options have to be accounted for.
 *
 * @param {*} event
 */
export async function chatAction(event) {
    console.log('chat.js chatAction - START', { event }, this);
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    // console.log("chat.js chatAction", { element, dataset });
    // const acLocation = element.closest('.card-buttons').querySelector('select[name="ac-location"]')?.value;
    const dmgAdjustment = element.closest('.card-buttons').querySelector('select[name="damage-adjustment"]')?.value;

    // get this chatCard ID incase we want to delete (used for undo buttons)
    const cmId = event.target.closest('[data-message-id]')?.dataset.messageId;
    const message = game.messages.get(cmId);

    // what command are we doing?
    const performAction = dataset.action;
    const parentDataset = element.parentNode.dataset;

    // console.log("chat.js chatAction", { parentDataset });

    // options specific to buttons
    const closest = event.currentTarget.closest('.card-buttons');
    const bCastSpell = !closest.classList.contains('chatCard-action');

    // console.log("item.js chatAction bCast", { event, element, dataset, performAction, parentDataset, bCastSpell });
    // an "action" record
    const actionId = parentDataset.actionId;
    // source Actor and Token Id if avaliable
    const sourceActorId = parentDataset.sourceActorId;
    const sourceTokenId = parentDataset.sourceTokenId;
    // Target token id if avaliable
    const targetTokenId = parentDataset.targetTokenId;
    // effect id (used to find and undo an effect application)
    const effectId = parentDataset.effectId;
    // item (weapon or spell) record id
    const itemId = parentDataset.itemId;
    // this is for modders that want to force large damage roll
    const forceLarge = dataset.forcelarge === 'true';

    let sourceActor, sourceToken, targetToken, targetActor;

    if (sourceTokenId) {
        sourceToken = canvas.tokens.get(sourceTokenId) || null;
        if (sourceToken) {
            sourceActor = sourceToken.actor;
        }
    }
    if (!sourceActor && sourceActorId) {
        sourceActor = game.actors.get(sourceActorId) || null;
    }
    if (!sourceToken) sourceToken = canvas.tokens.get(message?.speaker?.token);
    if (!sourceActor) sourceActor = game.actors.get(message?.speaker?.actor) || null;
    // console.log("item.js chatAction actors", { sourceActor, sourceToken });

    if (targetTokenId) {
        targetToken = canvas.tokens.get(targetTokenId) || null;
        targetActor = targetToken.actor;
    }

    // console.log("item.js chatAction datas", { sourceActor, sourceToken, targetToken, targetActor });

    if (!sourceActor) {
        // author had some issues with this with his module.
        if (!game.modules.get('roll-new-character-stats')?.active)
            ui.notifications.error(`Unable to find originating chat cards token/actor. [${sourceTokenId}/${sourceActorId}]`);
        return;
    }

    // these are values used for spells that are memorized
    const slotIndex = bCastSpell ? closest.dataset.index : undefined;
    const slotType = bCastSpell ? closest.dataset.type : undefined;
    const slotLevel = bCastSpell ? closest.dataset.level : undefined;

    let sourceItem = itemId ? sourceActor.items.get(itemId) : undefined;
    if (bCastSpell) {
        // if spell not in inventory check global list
        if (!sourceItem && slotType) {
            // item = await sourceActor._getSpellById(game.ars.library.spells[slotType].all, itemId);
            sourceItem = await utilitiesManager.getItem(itemId);
        }
    }

    // action item or from actor
    const sourceAction = sourceItem
        ? actionManager.getAction(sourceItem, actionId)
        : actionManager.getAction(sourceActor, actionId);

    const slotInfo = slotType
        ? {
              slotIndex,
              slotType,
              slotLevel,
          }
        : undefined;

    console.log('item.js chatAction', { sourceActor, sourceToken, sourceAction, sourceItem, performAction });

    switch (performAction) {
        case 'action-attack-roll':
            const diceAction = await new ARSDice(sourceToken?.object ?? sourceActor, sourceAction, { event }).makeActionAttack(
                slotInfo
            );
            break;

        case 'action-damage-roll':
            const diceActionDamage = await new ARSDice(sourceToken ?? sourceActor, sourceAction, {
                event: event,
            }).makeDamageRoll(true);

            break;

        case 'action-heal-roll':
            const diceActionHeale = await new ARSDice(sourceToken ?? sourceActor, sourceAction, {
                event: event,
            }).makeDamageRoll(false);

            break;

        case 'action-effect-roll':
            if (sourceActor) {
                effectManager.applyEffect(sourceToken ? sourceToken.actor : sourceActor, sourceAction);
            } else {
                ui.notifications.error(`Action effects require sourceActor.`);
            }
            break;

        case 'undo-action-effect':
            if (targetActor) {
                effectManager.undoEffect(targetActor, effectId);
                game.messages.get(cmId).delete();
            } else {
                ui.notifications.error(`Action requires a target.`);
            }
            break;

        case 'undo-healthadjust':
            if (game.user.isGM) {
                const isDamage = parentDataset.isDamage === 'true';
                const adjustment = parseInt(parentDataset.hpAdjustment);
                let hpPath = targetActor.system.attributes.hp;

                // if it was damage, then we heal, if it was heal, we damage
                const newValue = isDamage ? hpPath.value + adjustment : hpPath.value - adjustment;
                const nTargetHPResult = Math.clamped(newValue, hpPath.min, hpPath.max);

                await utilitiesManager.setActorHealth(targetActor, nTargetHPResult);
                if (targetToken.hasActiveHUD) canvas.tokens.hud.render();
                ui.notifications.notify(
                    (isDamage ? 'Restored' : 'Removed') + ` ${adjustment} health on ${targetActor.name}.`,
                    'info'
                );
                // delete the chatCard since we used undo button
                game.messages.get(cmId).delete();
            }
            break;

        case 'healthchange-roll':
            const diceWeaponDamage = await new ARSDice(sourceToken ?? sourceActor, sourceItem, { event: event }).makeDamageRoll(
                true
            );
            break;

        case 'cast-roll':
            const diceRollCast = await new ARSDice(sourceToken ?? sourceActor, sourceAction, { event: event }).makeCast(
                slotInfo
            );
            break;

        case 'cast-shape-template':
            PlaceCastShape.placeCastShape(sourceActor, sourceAction);
            break;

        case 'save-roll':
            const saveRollDiceCheck = new ARSDice(sourceToken ?? sourceActor, sourceAction, { event: event }).makeSaveRoll(
                true
            );

            break;

        case 'ability-roll':
            if (sourceAction) {
                let abilityCheckFormula = sourceAction.abilityCheck.formula
                    ? (await utilitiesManager.evaluateFormulaValueAsync(
                          this.action.abilityCheck.formula,
                          this.actor.getRollData()
                      )) || 'd20'
                    : 'd20';
                const autoAbilityCheckOnAttack = new ARSDice(sourceToken ?? sourceActor, sourceAction, {
                    event: event,
                }).makeAbilityCheckRoll(sourceAction.abilityCheck.type, abilityCheckFormula, true);
            }
            break;

        case 'skill-roll':
            const skillCheckRoll = await new ARSDice(sourceToken ?? sourceActor, sourceItem, {
                event: event,
            }).makeSkillRoll();
            console.log('chat.js chatAction skill-roll', { skillCheckRoll });
            break;

        case 'use-roll':
            const itemUsed = await utilitiesManager.useActionCharge(sourceActor, sourceAction, sourceItem);
            if (sourceItem) {
                if (itemUsed) {
                    utilitiesManager.chatMessage(
                        ChatMessage.getSpeaker({ actor: sourceActor }),
                        `${sourceItem.name}`,
                        `${sourceActor.name} used ${sourceItem.name}`,
                        dd.item.img
                    );
                } else {
                    utilitiesManager.chatMessage(
                        ChatMessage.getSpeaker({ actor: sourceActor }),
                        `${sourceItem.name}`,
                        `${sourceActor.name} tried to use <b>${sourceItem.name}</b> but no more uses remain.`,
                        sourceItem.img
                    );
                }
            } else {
                ui.notifications.error(
                    `No item found for this use. ${
                        /^Actor\.[^.]*$/.test(sourceAction.parentuuid) ? 'Action "use" does not work on actors' : ''
                    }.`
                );
            }
            break;

        default:
            console.log('item.js chatAction unknown performAction', { performAction });
            break;
    }
}
