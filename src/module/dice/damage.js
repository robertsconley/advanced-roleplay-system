import { ARSItem } from '../item/item.js';
import { ARSActor } from '../actor/actor.js';
import { ARSToken, ARSTokenDocument } from '../token/token.js';
import * as utilitiesManager from '../utilities.js';
import { CombatManager } from '../combat/combat.js';

/**
 * All the related info related to damage (not rolls)
 *
 * adjustments, resists/absorbs/etc and damage message
 *
 */
export class ARSDamage {
    /**
     *
     * Damage class
     *
     * @param {*} isDamage
     * @param {*} actorOrToken
     * @param {*} target
     * @param {*} attackObject
     * @param {*} damagetype
     * @param {*} roll
     * @param {*} options
     */
    constructor(isDamage, actorOrToken, target, attackObject, damagetype, roll, options) {
        this.isDamage = isDamage;
        this.options = options;
        this.roll = roll;
        this.isSelf = actorOrToken == target;

        if (actorOrToken instanceof ARSActor) {
            this.token = actorOrToken?.getToken() || undefined;
            this.actor = actorOrToken;
        } else if (actorOrToken instanceof ARSTokenDocument) {
            this.token = actorOrToken;
            this.actor = actorOrToken.actor;
        } else {
            this.token = actorOrToken;
            this.actor = actorOrToken.actor;
        }

        // this.token = token;
        this.target = target;
        this.weapon = attackObject instanceof ARSItem ? attackObject : undefined;
        // if it's not a item then it has to be Action.
        this.action = attackObject instanceof ARSItem ? undefined : attackObject;
        this.item = attackObject?.parentuuid ? fromUuidSync(attackObject.parentuuid) : this.weapon;

        this.damagetype = damagetype;
    }

    /**
     * This function applies damage adjustments to a target based on various factors.
     * It takes into account resistances, vulnerabilities, immunities, and damage absorption.
     * It returns an object containing the total damage, damage types, and absorbed damage.
     */
    async applyDamageAdjustments() {
        let damageTotalDone = this.roll.total; // Accumulator for total damage done
        let absorbedTotal = 0; // Accumulator for total damage absorbed
        if (this.target && !this.isSelf) {
            // Check for the existence of a save cache and if it applies to the current situation
            const saveCache = this.target ? await this.target.document.getFlag('world', 'saveCache') : undefined;
            const halfDamageFromSave = saveCache
                ? saveCache.save === 'halve' && saveCache.sourceId === String(this.token.actor.id)
                : false;

            // Process damage entries
            let rollTotal = this.roll.total; // Damage roll total for the current entry

            // Apply damage adjustments if bDamage is true and targetActor exists
            if (this.isDamage && this.target.actor) {
                rollTotal = this._processHalfDamageFromSave(rollTotal, halfDamageFromSave);
                rollTotal = this._processWeaponMagicAndMetalResists(rollTotal);
                rollTotal = this._processPerDiceResists(rollTotal);
                rollTotal = this._processResistsByAmount(rollTotal);
                rollTotal = this._processImmVulnResists(rollTotal);

                const absorbResults = await this._processAbsorbDamage(rollTotal);
                rollTotal = absorbResults.rollTotal;
                absorbedTotal = absorbResults.absorbedTotal;
            }

            // Delete save cache and reset clearSaveCache flag if necessary
            if (halfDamageFromSave) utilitiesManager.deleteSaveCache(this.target);

            // Update the total damage done
            damageTotalDone = Math.round(rollTotal);
        }

        this.totalDamage = damageTotalDone;
        this.totalAbsorbed = absorbedTotal;
        return this;
    }

    /**
     * Process half damage from saving throw.
     * @param {number} rollTotal - The total damage before processing half damage from saving throw.
     * @param {boolean} halfDamageFromSave - Indicates whether half damage should be applied based on a saving throw.
     * @return {number} The updated rollTotal after processing half damage from saving throw.
     */
    _processHalfDamageFromSave(rollTotal, halfDamageFromSave) {
        if (halfDamageFromSave) {
            return Math.round(rollTotal * 0.5);
        }
        return rollTotal;
    }

    /**
     * Process weapon magic and metal resistances to determine the adjusted roll total.
     *
     * @param {number} rollTotal - The initial roll total before adjustments.
     * @returns {number} - The adjusted roll total after applying resistances.
     */
    _processWeaponMagicAndMetalResists(rollTotal) {
        if (this.weapon) {
            const targetEffectFormula = utilitiesManager.getTriggerFormula(
                this.token.actor,
                this?.target?.actor,
                'target',
                '',
                'magicpotency',
                this.weapon
            );
            const attackerEffectFormula = utilitiesManager.getTriggerFormula(
                this?.target?.actor,
                this.token.actor,
                'target',
                '',
                'magicpotency',
                this.weapon
            );
            let magicPotencyFormula = targetEffectFormula;
            if (attackerEffectFormula && attackerEffectFormula.formula) {
                magicPotencyFormula = targetEffectFormula.concat(attackerEffectFormula);
            }
            const pontencyFormula = magicPotencyFormula ? magicPotencyFormula.formula.join(' + ') : '';
            let potencyFormulaValue = pontencyFormula
                ? utilitiesManager.evaluateFormulaValue(pontencyFormula, magicPotencyFormula.rollData)
                : 0;
            // const attackMods = {
            //     effects: {
            //         self: this.token.actor.getTargetCombatMods(this.token.actor, 'damage', this.weapon),
            //         target: this.target.actor?.getAttackerCombatMods(this.token.actor, 'damage', this.weapon),
            //     },
            // };

            // Determine if a special metal property is found in the weapon or action.
            let specialMetal = false;

            // Calculate target's magic potency resistance.
            let targetMagicPotency = this.target.actor.magicPotencyDefense();

            // Calculate item's magic potency.
            let itemPotency = this.weapon ? this.weapon.system.attack.magicPotency : 0;

            // Add ammunition's magic potency if it exists.
            if (this.weapon && this.weapon.ammo && this.weapon.ammo.system?.attack?.magicPotency) {
                itemPotency += this.weapon.ammo.system.attack.magicPotency;
            }

            // Calculate action's magic potency.
            const actionPotency = this.action ? this.action.magicpotency : 0;
            const actionProperties = this.action ? Object.values(this.action.properties) : [];
            const itemProperties = this.item ? Object.values(this.item.system.attributes.properties || []) : [];

            // Calculate the effective magic potency.
            let magicPotency = this.token.actor.magicPotencyOffense();
            if (actionPotency > magicPotency) magicPotency = actionPotency;
            if (itemPotency > magicPotency) magicPotency = itemPotency;
            // if (attackMods.effects.self.magicpotency) magicPotency += attackMods.effects.self.magicpotency;
            if (potencyFormulaValue) magicPotency += potencyFormulaValue;
            // Create a list of properties from the item and action.
            const propertiesList = itemProperties.concat(actionProperties).map((entry) => entry.trim().toLowerCase());

            // Check for metal resistances in the target.
            const metals =
                this.target.type === 'npc' ? Object.values(this.target.actor.system.resistances?.weapon?.metals) : undefined;
            if (metals && metals.length && propertiesList && propertiesList.length) {
                for (const metal of metals) {
                    if (propertiesList.includes(metal.type.toLowerCase().trim())) {
                        // Apply metal protection effects.
                        switch (metal.protection) {
                            case 'halve':
                                rollTotal = Math.round(rollTotal * 0.5);
                                break;
                            case 'full':
                                break;
                        }
                        if (!specialMetal) specialMetal = true;
                        break;
                    }
                }
            }

            // If no special metal is found and magic potency is less than target magic potency, set roll total to 0.
            if (!specialMetal && magicPotency < targetMagicPotency) {
                rollTotal = 0;
            }
        }
        return rollTotal;
    }

    /**
     * Process Per-dice Resistance Modifications
     *
     * @param {Object} entry - The entry object containing roll and damage type information.
     * @param {Object} targetActor - The target actor object containing system modifier information.
     * @param {Number} rollTotal - The initial roll total to be modified.
     * @returns {Number} - The modified roll total after applying per-dice resistance.
     */
    _processPerDiceResists(rollTotal) {
        // Store the resistPerDiceValue, if any.
        let resistPerDiceValue;

        // Check for a specific resistPerDiceValue in the targetActor's system modifiers.
        if (this.target.actor.system.mods.resists?.perdice?.[this.damagetype]) {
            resistPerDiceValue = this.target.actor.system.mods.resists.perdice[this.damagetype];
        }
        // If not found, check for a global resistPerDiceValue in the targetActor's system modifiers.
        else if (this.target.actor.system.mods.resists?.perdice?.all) {
            resistPerDiceValue = this.target.actor.system.mods.resists.perdice.all;
        }

        // Convert resistPerDiceValue to an integer, defaulting to 0 if not set.
        const resistsPerDice = parseInt(resistPerDiceValue || 0);

        // If resistsPerDice is a valid numeric value, apply the per-dice resistance modifications.
        if (resistsPerDice && Number.isFinite(resistsPerDice)) {
            // Reset rollTotal and store the original roll total and dice roll results for later use.
            rollTotal = 0;
            const perRollTotal = this.roll.total;
            const diceRollResults = this.roll.dice[0].total ? this.roll.dice[0].total : 0;
            const otherTerms = diceRollResults ? perRollTotal - diceRollResults : 0;

            // Iterate through all dice in the roll.
            for (const dieRoll of this.roll.dice) {
                // Iterate through each individual result of the die roll.
                for (const perRolled of dieRoll.results) {
                    // Update rollTotal by adding the maximum of the modified roll result and 1.
                    rollTotal += Math.max(perRolled.result + parseInt(resistsPerDice), 1);
                }
            }
            // Add otherTerms back to the rollTotal.
            rollTotal += otherTerms;
        }

        return rollTotal;
    }

    /**
     * Process damage resistance for a target actor based on the given damage type.
     *
     * @param {number} rollTotal - The initial damage roll total.
     * @returns {number} - The adjusted roll total after processing resistance.
     */
    _processResistsByAmount(rollTotal) {
        let resist;

        // Safely retrieve the resist value from the target actor's system mods.
        try {
            resist = this.target.actor.system.mods.resists[this.damagetype];
        } catch (error) {
            // No action needed, resist remains undefined.
        }

        // Check if resist value is valid and not zero.
        if (resist && !isNaN(resist) && parseInt(resist) !== 0) {
            const amount = parseInt(resist);
            const resistPercentage = amount * 0.01; // Convert resist amount to percentage multiplier.
            const adjustedDamage = Math.round(rollTotal * resistPercentage); // Calculate adjusted damage.

            rollTotal -= adjustedDamage; // Reduce rollTotal by the adjusted damage.
        }

        return rollTotal; // Return the updated rollTotal.
    }

    /**
     * Process immunities, resistances, and vulnerabilities for a target token based on a given damage type.
     *
     * @param {number} rollTotal - The initial damage roll total.
     * @returns {number} - The modified damage roll total after processing immunities, resistances, and vulnerabilities.
     */
    _processImmVulnResists(rollTotal) {
        // Helper function to apply resistance/immunity/vulnerability modifications to the damage roll total
        function _resistHelper(rollTotal, resistanceType = 'resist') {
            // console.log("combat.js processImmVulnResists _resistHelper", { targetActor, rollTotal, resistanceType, dmgType });

            let newTotal = rollTotal; // Initialize the new total as the original roll total

            // Search for a matching effect
            findOneMatch: for (const effect of this.target.actor.getActiveEffects()) {
                for (const change of effect.changes) {
                    if (change.key === `system.mods.${resistanceType}`) {
                        // Parse resistances and compare them to the current damage type
                        const resists = change.value
                            .toLowerCase()
                            .split(',')
                            .map((text) => text.trim());
                        if (resists.includes('all') || resists.includes(this.damagetype.toLowerCase())) {
                            // Apply the appropriate resistance/immunity/vulnerability modification
                            switch (resistanceType) {
                                case 'immune':
                                    newTotal = 0;
                                    break;
                                case 'resist':
                                    newTotal = Math.round(rollTotal * 0.5);
                                    break;
                                case 'vuln':
                                    newTotal = Math.round(rollTotal * 2);
                                    break;
                            }
                            // We only match one effect, then exit the loop
                            break findOneMatch;
                        }
                    }
                }
            }
            return newTotal; // Return the updated total after applying the resistance/immunity/vulnerability modification
        }

        // Apply immunities, resistances, and vulnerabilities
        rollTotal = _resistHelper.bind(this)(rollTotal, 'immune');
        rollTotal = _resistHelper.bind(this)(rollTotal, 'resist');
        rollTotal = _resistHelper.bind(this)(rollTotal, 'vuln');

        return rollTotal; // Return the updated rollTotal after applying all modifications
    }

    /**
     * Processes the absorption of damage for a given target, source, and damage type.
     *
     * @param {*} rollTotal - The total amount of damage being dealt.
     * @returns - An object containing the updated rollTotal and the total absorbed damage.
     */
    async _processAbsorbDamage(rollTotal) {
        let absorbedTotal = 0; // Total absorbed damage.
        let depletedEffect = []; // Array of depleted effect IDs.
        let updatedEffects = false; // Flag to check if any effects have been updated.

        let effectsBundle = duplicate(this.target.actor.effects);
        effectsBundle.forEach((effect, efindex) => {
            if (!effect.disabled && !effect.isSuppressed) {
                effect.changes.forEach((change, index) => {
                    if (rollTotal > 0 && change.key === 'special.absorb') {
                        const rollTotalOrig = rollTotal;
                        const details = JSON.parse(change.value.toLowerCase());
                        details.amount = parseInt(details.amount);
                        if (details.amount && details.damagetype) {
                            updatedEffects = true;
                            if (
                                details.damagetype === this.damagetype ||
                                details.damagetype === 'none' ||
                                details.damagetype === 'all'
                            ) {
                                let amountleft = details.amount; // Remaining absorption amount for the current effect.
                                const absorbDiff = rollTotal - details.amount;
                                if (absorbDiff < 0) {
                                    // absorbed it all, save the rest
                                    amountleft = Math.abs(absorbDiff);
                                    absorbedTotal += rollTotal;
                                    rollTotal = 0;
                                } else {
                                    rollTotal = absorbDiff;
                                    amountleft = 0;
                                    depletedEffect.push(effect._id);
                                    absorbedTotal += details.amount;
                                }
                                details.amount = amountleft;
                                change.value = JSON.stringify(details);
                                if (details.damagetype === 'none') {
                                    // a none absorb effect only tracks absorption but doesn't absorb the damage.
                                    rollTotal = rollTotalOrig;
                                    absorbedTotal = 0;
                                }
                            }
                        }
                    }
                });
            }
        });

        if (updatedEffects) {
            await utilitiesManager.runAsGM({
                sourceFunction: 'applyDamageAdjustments',
                operation: 'actorUpdate',
                user: game.user.id,
                targetTokenId: this.target.id,
                update: { effects: effectsBundle },
            });
        }

        if (depletedEffect.length) {
            await utilitiesManager.runAsGM({
                operation: 'deleteActiveEffect',
                user: game.user.id,
                targetActorId: this.target.actor.id,
                targetTokenId: this.target.id,
                sourceActorId: this.token.actor.id,
                effectIds: depletedEffect,
            });
        }

        return {
            rollTotal, // Return the updated rollTotal
            absorbedTotal, // Return the absorbedTotal
        };
    }

    /**
     *
     * Apply the actual damage taken to the actor
     *
     * @returns {Number} hp difference
     */
    applyDamageToActor() {
        if (!this.target && !this.isSelf) return 0;
        const target = this.isSelf ? this.actor : this.target?.actor;

        //TODO: allow below 0 for PCs?
        let hpAdjustment = Math.max(this.totalDamage, 0);
        let hpDifference = 0,
            updatedHP;
        if (target) {
            const hpAttributes = target.system.attributes.hp;
            const originalHP = hpAttributes.value;

            // Apply damage or healing, ensuring HP stays within defined bounds
            updatedHP = this.isDamage
                ? Math.clamped(originalHP - hpAdjustment, hpAttributes.min, hpAttributes.max)
                : Math.clamped(originalHP + hpAdjustment, hpAttributes.min, hpAttributes.max);

            hpDifference = Math.abs(originalHP - updatedHP);
        }

        utilitiesManager.runAsGM({
            sourceFunction: 'damage.js applyDamageToActor',
            operation: 'adjustTargetHealth',
            user: game.user.id,
            targetTokenId: this?.target?.id,
            targetActorId: this?.target?.id,
            targetHPresult: updatedHP,
        });

        if (this.isDamage) CombatManager.armorDamage(this);
        return hpDifference;
    }
    /**
     *
     * Create chat card message with details, using hpdiff for undo click
     *
     * @param {Number} hpDifference
     * @param {String} flavor
     */
    async sendDamageChatCard(hpDifference, flavor = '') {
        const damageTypeLocalized = this.isDamage ? game.i18n.localize('ARS.damageTypes.' + this.damagetype) : '';

        const diceHTMLToolTips = await this.roll.getTooltip();
        let context =
            `<div data-id="${this.target?.id}" class="secure-name">${this.actor.name}</div>` +
            `<a data-value="${this.roll.total}" class="secure-value">${this.totalDamage}</a> ${damageTypeLocalized}${
                this.isDamage ? ' damage' : ' healing'
            }${flavor ? ` ${flavor.toUpperCase()}` : ''}`;

        if (this.target) {
            context =
                `<div data-id="${this.target?.id}" class="secure-name">${this.target?.name}</div>` +
                `<a data-value="${this.roll.total}" class="secure-value">${this.totalDamage}</a> ` +
                `${damageTypeLocalized}${this.isDamage ? ' damage' : ' healing'}` +
                `${flavor ? `<div>${flavor.toUpperCase()}</div>` : ''}`;
        }

        let speaker = ChatMessage.getSpeaker({ actor: this.actor });
        let cardData = {
            speaker: speaker,
            flavor: context,
            tooltip: this.roll.resultt,
            isDamage: this.isDamage,
            adjustment: hpDifference,
            roll: this.roll,
            sourceActorId: this.token?.actor?.id,
            targetActorId: this.target?.actor?.id,
            targetToken: this.target,
            owner: this.actor.id,
            game: game,
        };

        const content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-healthAdjust.hbs', cardData);

        let chatData = {
            content: content,
            user: game.user.id,
            speaker: speaker,
            rollMode: this.roll.rollMode,
            flags: {
                world: {
                    damage: this.isDamage ? -this.totalDamage : this.totalDamage,
                    damageType: this.damagetype,
                },
            },
            // roll: roll,
        };
        ChatMessage.applyRollMode(chatData, this.roll.rollMode);
        ChatMessage.create(chatData);

        //Check is the target his was casting and warn if interrupted
        if (this.isDamage && this.target?.combatant && this.target.combatant.getFlag('world', 'initCasting')) {
            const combat = this.target.combatant.combat;
            const currentInitiative = combat._getCurrentTurnInitiative();
            // only show message if target initiative hasnt passed yet
            if (this.target.combatant.initiative && currentInitiative < this.target.combatant.initiative) {
                utilitiesManager.chatMessage(
                    ChatMessage.getSpeaker({ actor: this.target.actor }),
                    'Casting Interrupted',
                    `${this.target.name} has casting interrupted`,
                    this.target.img,
                    { rollMode: this.roll.rollMode }
                );
            }
        }
    } // end makeHealthAdjustChatCard
}
