import { ARS } from '../config.js';
import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';
import * as initLibrary from '../library.js';

export class ARSActorBrowserManager {
    static addActorBrowserButton(app, html) {
        // console.log("actor-browser.js addBrowserButton", { app, html })
        if (game.user.isGM && (app.id == 'compendium' || app.id == 'actors')) {
            const browserButton = $(
                `<button class='actor-browser' data-tooltip='Browse Actors'> ` +
                    `<i class='fas fa-users'></i>` +
                    `Actor Browser` +
                    `</button>`
            );

            browserButton.click(function (env) {
                console.log('actor-browser.js browserButton', { env });
                if (!game.ars.ui.actorbrowser) {
                    // if (!game?.ars?.library?.packs?.actors)
                    //     ui.notifications.warn(`Actor browser is loading data...please wait.`);
                    // return;
                    game.ars.ui = {
                        actorbrowser: new ARSActorBrowser(),
                    };
                }
                game.ars.ui.actorbrowser.render(true);
            });

            // html.find(".directory-header").append(browserButton);
            html.find('.header-search').before(browserButton);
        }
    }
}
export class ARSActorBrowser extends Application {
    constructor(app) {
        super(app);
        this.actors = [];

        // Initialize filters object with default values and optional listOptions for future implementation
        this.initFilters();
    }

    /**
     * Initialize filters object
     * - The idea behind listOptions (not used right now) is that we might add a dropdown instead of free form text
     * - Each time a filter is run, it would build the dropdown based on that field's options
     * - But, free form gives the option to use regex so... not sure, will see
     */
    initFilters() {
        this.filters = {
            type: 'npc', // Filter by npc type
            source: 'all', // Filter by source, default is 'All'
            general: {
                name: '', // Filter by name
                description: '', // Filter by description
            },
            npc: {
                'details.type': {
                    value: '',
                },
                'details.alignment': {
                    value: '',
                },
                climate: {
                    value: '',
                },
                frequency: {
                    value: '',
                },
                hitdice: {
                    value: '',
                },
                organization: {
                    value: '',
                },
                diet: {
                    value: '',
                },
                activity: {
                    value: '',
                },
                intelligence: {
                    value: '',
                },
            },
        };
    }

    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            resizable: true,
            minimizable: true,
            id: 'actor-browser-sheet',
            classes: ['ars', 'actor-browser'],
            title: 'Actor Browser',
            template: 'systems/ars/templates/apps/actor-browser.hbs',
            // width: 700,
            // height: 830,
            scrollY: ['.filter-list', '.item-list'],
            dragDrop: [
                {
                    dragSelector: '.item-entries .filter-name',
                    dropSelector: null,
                },
            ],
        });
    }

    /** @override */
    async getData() {
        const data = await super.getData();

        data.documentTypes = ['all', ...game.system.documentTypes.Actor];
        data.this = this;
        data.filters = this.filters;
        data.game = game;
        data.config = ARS;

        data.actors = this.actors;

        // filter item list by source
        if (this.filters.source && this.filters.source !== 'all') {
            data.actors = data.actors.filter((i) => {
                return (this.filters.source === 'world' && !i.pack) || i.pack === this.filters.source;
            });
        }
        // filter item list by name
        data.actors = data.actors.filter(
            (i) =>
                (i.type === this.filters.type || this.filters.type === 'all') &&
                (!this.filters.general.name || i.name.match(new RegExp(`${this.filters.general.name}`, 'ig')))
        );

        // if this item type has special filters...
        if (this.filters[this.filters.type]) {
            const specialFilters = this.filters[this.filters.type];
            for (const filter of Object.keys(specialFilters)) {
                const value = String(specialFilters[filter].value);
                // console.log("actor-browser.js getData", { filter, value })
                if (value) {
                    data.actors = data.actors.filter((i) => {
                        const dataValue = getProperty(i.system, filter);
                        return dataValue && String(dataValue)?.match(new RegExp(`${value}`, 'ig'));
                    });
                }
            }
        }
        // end special filters

        const sourceList = [
            { label: 'All', value: 'all' },
            { label: 'World', value: 'world' },
        ];
        const sources = this.actors
            .filter((i) => i.pack)
            .forEach((m) => {
                if (!sourceList.some((obj) => obj.value === m.pack)) {
                    const pack = game.packs.get(m.pack);
                    sourceList.push({
                        label: pack.metadata.label,
                        value: m.pack,
                    });
                }
            });
        // set the source filter to a readable name
        data.filters.sourceName = sourceList.find((entry) => entry.value === this.filters.source).label || 'MISSING';

        data.sourceList = sourceList;
        // console.log("actor-browser.js getData", { data })
        return data;
    }

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        let searchEvents = html.find('.filter-item-type, .filter-name, .filter-source'),
            searchSubmit = html.find('.filter-search'),
            itemEdit = html.find('.item-edit'),
            refreshCompendiums = html.find('.refresh-compendiums');

        searchEvents.change((event) => this._searchGeneral(event));
        searchSubmit.click((event) => this._performSearch(event));
        refreshCompendiums.click(async (event) => {
            initLibrary.buildPackItemList().then(() => {
                game.ars.ui.actorbrowser.render(true);
            });
        });

        itemEdit.click(async (ev) => {
            const li = $(ev.currentTarget).parents('.item');
            const itemId = li.data('id');
            const itemPack = li.data('pack');
            let item;
            if (itemPack && itemId) {
                item = await game.packs.get(itemPack).getDocument(itemId);
            } else {
                item = game.actors.get(itemId);
            }
            if (!item) {
                ui.notifications.error(`Actor ${itemId} cannot be found in inventory.`);
                return;
            }
            item.sheet.render(true);
        });

        // add listeners for the specific fields
        for (const tag of game.system.documentTypes.Item) {
            if (this.filters[tag]) {
                const specialFilters = this.filters[tag];
                for (const filter of Object.keys(specialFilters)) {
                    const nodotsFilter = filter.replace(/\./g, '_');
                    html.find(`.filter-field-${nodotsFilter}`).change((event) => this._performSearch(event));
                }
            }
        }
    } // end activeListeners

    _onDragStart(event) {
        const li = event.currentTarget.closest('li');
        if (!li) return;
        const actorUuid = li.getAttribute('data-uuid');
        const actorId = li.getAttribute('data-id');
        console.log('actor-browser.js _onDragStart', { actorUuid });
        // const actor = fromUuidSync(actorUuid);
        // if (actor) {
        // dragData of actor
        const dragData = {
            type: 'Actor',
            id: actorId,
            uuid: actorUuid,
        };
        console.log('actor-browser.js _onDragStart', { actorUuid, actorId });
        event.dataTransfer.setData('text/plain', JSON.stringify(dragData));
        // }
    }

    _onDragDrop(event) {
        // console.log('actor-browser.js _onDragDrop', { event });
    }
    _canDragStart(event) {
        return game.user.isGM;
    }
    _canDragDrop(event) {
        return false;
    }

    /** @override to add item population */
    async _render(force = false, options = {}) {
        let worldItems = [];
        // const packItems = await utilitiesManager.getPackItems('Item', game.user.isGM);
        if (!game?.ars?.library?.packs?.actors) {
            ui.notifications.warn(`Actor browser is loading pack data...please wait.`);
            return;
        }
        const packItems = game.ars.library.packs.actors;
        // console.log("actor-browser.js _render", { packItems });

        if (game.user.isGM) {
            worldItems = game.actors.contents;
        } else {
            worldItems = game.actors.contents.filter((i) => i.permission > 1);
        }
        this.actors = worldItems.concat(packItems);
        this.actors.sort(utilitiesManager.sortByRecordName);
        await super._render(force, options);
    }

    _searchGeneral(event) {
        // console.log("actor-browser.js _searchGeneral", { event }, this)
        event.preventDefault();
        const element = event.currentTarget;
        const filterFields = element.closest('.filter-fields');

        const itemFilter = filterFields.querySelector('select[name="item-type"]')?.value;
        const sourceFilter = filterFields.querySelector('select[name="filter-source"]')?.value;
        const nameFilter = filterFields.querySelector('input[name="filter-name"]')?.value;

        if (itemFilter) {
            this.filters.type = itemFilter;
            this.filters.source = sourceFilter;
            this.filters.general.name = nameFilter;
            this.render();
        }
    }

    /**
     *
     * Apply current filters to item list
     *
     * @param {*} event
     */
    _performSearch(event) {
        console.log('actor-browser.js _performSearch', { event }, this);
        event.preventDefault();
        const element = event.currentTarget;
        const filterFields = element.closest('.filter-fields');

        //special filters
        if (this.filters[this.filters.type]) {
            for (const filter of Object.keys(this.filters[this.filters.type])) {
                const nodotsFilter = filter.replace(/\./g, '_');
                const value = filterFields.querySelector(`input[name="filter-field-${nodotsFilter}"]`)?.value;
                this.filters[this.filters.type][filter].value = value;
            }
            // console.log("actor-browser.js _performSearch ==>", this.filters[this.filters.type])
        }
        //

        this.render();
    }
}
