import { ARS } from '../config.js';

/**
 * Actions that are attached to items or actors, basically in anything
 * 
 * This class will take the old action object and put it into 
 * the class and it will become a ARSAction object
 * 
 *       "action":{
         "img":"",
         "templates":[
            "itemDescription"
         ],
         "ability":"none",
         "type":"cast",
         "targeting":"",
         "successAction":"",
         "formula":"",
         "otherdmg":[
            
         ],
         "effectList":[
            
         ],
         "effect":{
            "duration":{
               "formula":"",
               "type":"round"
            },
            "changes":[
               
            ]
         },
         "speed":0,
         "damagetype":"slashing",
         "resource":{
            "type":"none",
            "itemId":"",
            "reusetime":"",
            "count":{
               "cost":1,
               "min":0,
               "max":1,
               "value":0
            }
         },
         "magicpotency": 0,
         "properties":[]
      },
 * 
 */
export class ARSAction {
    /**
     *
     * @param {Object} source actor, item
     * @param {Number} atIndex where in the actions list should this be placed?
     * @param {String} inGroup use name of existing group
     */
    constructor(source, atIndex = 0, inGroup = '') {
        // this.img = actionObject.img;
        // this.ability = actionObject.ability;
        // this.type = actionObject.type;
        // this.successAction = actionObject.successAction;
        // this.formula = actionObject.formula;
        // this.otherdmg = actionObject.otherdmg;
        // this.speed = actionObject.speed;
        // this.damagetype = actionObject.damagetype;
        // this.magicpotency = actionObject.magicpotency;

        // this.properties = duplicate(actionObject.properties);
        // this.effectList = duplicate(actionObject.effectList);
        // this.effect = duplicate(actionObject.effect);
        // this.resource = duplicate(actionObject.resource);
        let action = duplicate(game.system.template.Item['action']);
        if (action.templates instanceof Array) {
            action.templates.forEach((t) => {
                action = mergeObject(action, game.system.template.Item.templates[t]);
            });
        }

        // add save and ability check templates into action as well, will be action[x].save/action[x].abilityCheck
        action = mergeObject(action, game.system.template.Item['saveCheck']);
        action = mergeObject(action, game.system.template.Item['castShapeProperties']);
        action = mergeObject(action, game.system.template.Item['abilityCheck']);
        action.id = randomID(16);
        delete action.templates;

        action.index = atIndex;
        if (inGroup) {
            action.name = inGroup;
        } else if (source && source.name) {
            action.name = source.name;
        } else {
            action.name = 'unknown';
        }
        if (source && source.img) {
            action.img = source.img;
        } else {
            // set the default image on action if object unset, to "cast" as it's the default new action type.
            action.img = ARS.icons.general.combat.cast;
        }

        action.parentuuid = source.uuid;

        Object.assign(this, action);
    }

    createFromAction(actionObject) {
        // Copy all enumerable properties from actionObject to this
        if (typeof actionObject !== 'object' || actionObject === null) {
            throw new Error('Invalid actionObject provided to ARSAction createFromAction');
        }

        // Copy all enumerable properties from actionObject to this
        Object.assign(this, actionObject);

        // Deep clone specific properties
        this.properties = foundry.utils.deepClone(actionObject.properties);
        this.effectList = foundry.utils.deepClone(actionObject.effectList);
        this.effect = foundry.utils.deepClone(actionObject.effect);
        this.resource = foundry.utils.deepClone(actionObject.resource);
    }
}
