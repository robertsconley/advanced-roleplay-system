import * as utilitiesManager from '../utilities.js';
import * as dialogManager from '../dialog.js';
import * as debug from '../debug.js';

export class CombatManager {
    /**
     * system variant 2 uses armor damage system, 1 armor damage per die of damage
     *
     * @param {Object} damage damage class object
     * @returns
     */
    static armorDamage(damage) {
        if (damage.roll.variant !== '2' || !game.ars.config.settings.useArmorDamage) {
            return; // Early return for conditions not met
        }

        const armorDamageTotal = damage.isDamage ? damage.roll.dice[0]?.results?.length || 0 : 0;
        if (armorDamageTotal === 0 || !damage.target?.actor?.armors) {
            return; // No damage to process or no armors to apply damage
        }

        damage.target.actor.armors.forEach((armor) => {
            const currentAP = armor.system.protection.points.value;
            if (currentAP > 0) {
                const newAP = Math.max(0, currentAP - armorDamageTotal);
                armor.update({ 'system.protection.points.value': newAP });
                if (newAP < 1) {
                    dialogManager.showNotification(
                        `Your <b>${armor.name}</b> is no longer functional.`,
                        'OK',
                        'Protection Broken'
                    );
                }
            }
        });
    }
}
