import * as debug from '../debug.js';
import { prepareActiveEffectCategories } from '../effect/effects.js';

/**
 * Class for Combat Hud
 *
 */

export class ARSCombatHUD extends Application {
    constructor(token, options = {}) {
        super(options);
        this.token = token;
        this.actor = token.actor;
        token.actor.apps[this.appId] = this;
        if (token.combatHud) this.#closepopoutCards();
        token.combatHud = this;
        token.popoutCards = [];
    } // end constructor

    /** @override */
    // static documentName = 'ARSCombatHUD';

    /** @override */
    static get defaultOptions() {
        return foundry.utils.mergeObject(super.defaultOptions, {
            // id: randomID(16),
            classes: ['ars', 'ars-combat-hud'],
            template: 'systems/ars/templates/apps/combat-hud-mini.hbs',
            title: 'Combat HUD',
            height: 'auto',
            width: 600,
            resizable: true,
            // scollY: ['ars-combat-hud'],
            dragDrop: [
                {
                    dragSelector: '.mini-weapon-list .mini-action-button',
                    dropSelector: null,
                },
                // { dragSelector: '.memorization-slot' },
                // { dragSelector: '.actionCard-roll .rollable', dropSelector: null },
            ],
            // dragDrop: [{ dropSelector: 'action-block' }],
        });
    } // end defaultOptions  d

    get id() {
        // const defaultId = game.user.isGM ? game.user.id : `ars-combat-hud-${this.token.id}`;
        const defaultId = `ars-combat-hud-${this.token.id}`;
        return defaultId;
    }

    get title() {
        const move = this.actor.type === 'npc' ? this.actor.system.movement : `${this.actor.system.attributes.movement.value}`;
        // : `${this.actor.system.attributes.movement.value} ${this.actor.system.attributes.movement.unit}`;

        return `${this.actor.name}, AC:${this.actor.system.armorClass.normal} / HP:${this.actor.system.attributes.hp.value} / MV:${move}`;
    }

    getData() {
        const effects = this.actor.getActiveEffects().filter((eff) => eff.isTemporary);
        const context = {
            token: this.token,
            actor: this.actor,
            system: this.actor.system,
            actions: this.actor.system.actions,
            actionList: this.actor.system.actionList,
            actionCount: Object.keys(this.actor.system.actionList).length,
            effects: effects,
            hasTempEffects: effects.length,
        };

        return context;
    } // end getData

    activateListeners(html) {
        super.activateListeners(html);

        const btnMiniHud = html.find('.chatCard-roll, .spellCard-roll');
        const btnMiniHudAction = html.find('.actionCard-roll');
        // Actions...
        btnMiniHudAction.click((e) => {
            this.actor.sheet._actionChatRoll.bind(this.actor.sheet)(e);
        });

        // weapon/skills/spells...
        btnMiniHud.click((e) => {
            this.actor.sheet._itemChatRoll.bind(this.actor.sheet)(e);
        });
    } // end activateListeners

    _onDragStart(event) {
        console.log('combat-hud.js _onDragStart', { event });
        // const rootAttr = event.target.getAttribute('root');
        const itemId = event.target.getAttribute('data-id');
        console.log('combat-hud.js _onDragStart', { itemId });
        const item = this.actor.getEmbeddedDocument('Item', itemId);
        if (item) {
            console.log('combat-hud.js _onDragStart', { item });
            event.dataTransfer.setData(
                'text/plain',
                JSON.stringify({
                    type: 'Item',
                    actorId: this.actor.id,
                    actorUuid: this.actor.uuid,
                    id: item.id,
                    uuid: item.uuid,
                    macroData: {
                        type: item.type,
                        img: item.img,
                        name: item.name,
                    },
                })
            );
        }
    }

    _onDragDrop(event) {
        // console.log('combat-hud.js _onDragDrop', { event });
    }
    _canDragStart(event) {
        // console.log('combat-hud.js _canDragStart', { event });
        return true;
    }
    _canDragDrop(event) {
        // console.log('combat-hud.js _canDragDrop', { event });
        return false;
    }

    createHud() {
        const token = this.token;
        const actor = this.actor;
        if (!actor || (!actor.isOwner && !game.user.isGM)) return;

        // position the hud below the selected token
        const viewportWidth = document.documentElement.clientWidth;
        const viewportHeight = document.documentElement.clientHeight;

        let hudTopPosition,
            hudLeftPostion,
            hudWidth = this?.position?.width || 600;
        if (game.settings.get('ars', 'floatingHudStaticPosition')) {
            const hudBottomOffset = 100;
            const element = document.getElementById('ui-bottom');
            const position = element.getBoundingClientRect();

            hudTopPosition = position.top - hudBottomOffset;
            hudLeftPostion = position.left;
            let hudTop = undefined,
                hudLeft = undefined;
            const saveKey = `${game.user.id}-combat-hud-location`;
            try {
                [hudTop, hudLeft, hudWidth] = JSON.parse(localStorage.getItem(saveKey));
            } catch (err) {}

            if (!hudWidth) hudWidth = this?.position?.width || 600;
            // console.log('OPENING.....localStorage', { hudTop, hudLeft });

            if (hudTop || hudLeft) {
                // if previous setting is good, we use it, otherwise we use default
                if (hudLeft > 0 && hudLeft < viewportWidth && hudTop < viewportHeight) {
                    hudTopPosition = hudTop;
                    hudLeftPostion = hudLeft;
                }
            }
        } else {
            const tokenWidth = Math.round(token.w * canvas.stage.scale.x),
                tokenHeight = Math.round(token.h * canvas.stage.scale.y),
                left = Math.round(token.worldTransform.tx),
                top = Math.round(token.worldTransform.ty),
                right = left + tokenWidth,
                bottom = top + tokenHeight;

            const tokenHudOffset = 35;
            hudTopPosition = bottom + tokenHudOffset;
            hudLeftPostion = left;

            //to far down
            if (bottom + tokenHudOffset > viewportHeight) {
                hudTopPosition = top + tokenHudOffset;
            }
            //to far left
            if (left < 0) {
                hudLeftPostion = right;
            }
            //to far right
            if (left + 100 > viewportWidth) {
                hudLeftPostion = Math.round(viewportWidth / 2);
            }
        }
        // console.log('OPENING.....WINDOW', { hudLeftPostion, hudTopPosition });
        this.element.css({ left: hudLeftPostion, top: hudTopPosition });
        // this.setPosition({ left: hudLeftPostion, top: hudTopPosition, height: 'auto', width: 600, zIndex: 100, scale: 1 });
        this.position.left = hudLeftPostion;
        this.position.top = hudTopPosition;
        this.position.width = hudWidth;

        this.render(true);
        return this;
    } // end createHud

    #saveWindowPosition() {
        if (this.rendered) {
            const windowX = this.position.left;
            const windowY = this.position.top;
            const windowW = this.position.width;

            // console.log('Saving Window Position...', { windowX, windowY }, this.position);
            const saveKey = `${game.user.id}-combat-hud-location`;

            localStorage.setItem(saveKey, JSON.stringify([windowY, windowX, windowW]));
        }
    }

    #closepopoutCards() {
        // this will close any actions still opened from this hud that was opened when the hud was up.
        if (this?.token?.popoutCards?.length) {
            const removePopouts = foundry.utils.deepClone(this.token.popoutCards);
            for (let i = 0; i < removePopouts.length; i++) {
                const popout = removePopouts[i];
                popout.close();
            }
        }
    }

    async close() {
        this.#saveWindowPosition();
        this.#closepopoutCards();

        delete this?.token?.actor?.apps?.[this.appId];
        delete this?.token?.combatHud;
        delete this?.token?.popoutCards;

        await super.close({ force: true });
        //for some reason super.close() doesn't seem to shut it down
        // entirely when the automated initiative target goes to next
        // actor for GM. This fixed that tho im sure there is a reason to not do this.
        delete this;
    }
}
