import { ARS } from '../config.js';
import { ARSItem } from '../item/item.js';
import { ARSActor } from '../actor/actor.js';
import { ARSToken, ARSTokenDocument } from '../token/token.js';
import * as utilitiesManager from '../utilities.js';
import * as effectManager from '../effect/effects.js';
import { CombatManager } from '../combat/combat.js';
import * as dialogManager from '../dialog.js';
import { ARSDamage } from './damage.js';
import * as debug from '../debug.js';
import {
    ARSRoll,
    ARSRollAttack,
    ARSRollBase,
    ARSRollCombat,
    ARSRollDamage,
    ARSRollSave,
    ARSRollInitiative,
    ARSRollSkill,
    ARSRollAbilityCheck,
} from './rolls.js';

/**
 *
 * Attack with weapon, action, make save, check, cast spell
 *
 * Had to try and make this work as best I could without
 * token/target for folks that dont play with maps. It will
 * allow rolls from sheets and general messages in chat.
 *
 */
export class ARSDice {
    /**
     *
     * @param {*} token
     * @param {*} attackObject
     * @param {*} options
     */
    constructor(actorOrToken, attackObject, options = {}) {
        // console.log('dice.js ARSDice constructor', { actorOrToken, attackObject, options });

        if (actorOrToken instanceof ARSActor) {
            this.token = actorOrToken?.getToken()?.object || undefined;
            this.actor = actorOrToken;
        } else if (actorOrToken instanceof ARSTokenDocument) {
            this.token = actorOrToken.object;
            this.actor = actorOrToken.actor;
        } else if (actorOrToken instanceof ARSToken) {
            this.token = actorOrToken;
            this.actor = actorOrToken.actor;
        } else {
            // ui.notifications.error(`dice.js ARSDice Unknown actor or token instance for [${actorOrToken?.name}]`);
            // throw new Error(`dice.js ARSDice Unknown actor or token instance for [${actorOrToken?.name}]`);
            this.token = actorOrToken.object;
            this.actor = actorOrToken.actor;
        }

        this.options = options;

        this.weapon = attackObject instanceof ARSItem && attackObject.type === 'weapon' ? attackObject : undefined;
        // if it's not a item then it has to be Action.
        this.action = attackObject instanceof ARSItem ? undefined : attackObject;
        this.item = attackObject?.parentuuid ? fromUuidSync(attackObject.parentuuid) : this.weapon;
        this.skill = attackObject instanceof ARSItem && attackObject.type === 'skill' ? attackObject : undefined;

        this.roll = undefined;
        this.dmgRoll = undefined;
        this.dmgOther = [];
        // console.log('dice.js ARSDice constructor', this);
    }

    /**
     * Cast a spell from a item/action
     *
     * @param {*} isSpell
     * @param {*} slotInfo
     * @returns
     */
    async makeCast(slotInfo) {
        // console.log('dice.js makeCast', { slotInfo });
        const isSpellSlot = slotInfo != null;
        const forceSaveCheck = this.action.saveCheck.type !== 'none';
        const forceAbilityCheck = this.action.abilityCheck.type !== 'none';

        if (
            isSpellSlot &&
            utilitiesManager.isMemslotUsed(this.actor, slotInfo.slotType, slotInfo.slotLevel, slotInfo.slotIndex)
        ) {
            console.log('dice.js makeCast spell slot used already!');
            ui.notifications.warn(`${this.item.name} has no more uses left.`);
            if (!game.user.isGM) return;
        } else if (
            !isSpellSlot &&
            this.action &&
            game.ars.config.chargedActions.includes(this.action.type) &&
            this.action.resource.type != 'none'
        ) {
            if (!(await utilitiesManager.useActionCharge(this.actor, this.item, this.action))) {
                const resourceUsed = game.i18n.localize(game.ars.config.consumed[this.action.resource.type] ?? 'uses');
                console.log(`dice.js rollAttack ${this.action.name} has no more ${resourceUsed} left.`);
                ui.notifications.warn(`${this.action.name} has no more ${resourceUsed} left.`);
                if (!game.user.isGM) return;
            }
        }

        if (forceSaveCheck) {
            const autoSaveOnAttack = new ARSDice(this.actor, this.action, this.options).makeSaveRoll(true);
        } else if (forceAbilityCheck) {
            let abilityCheckFormula = this.action.abilityCheck.formula
                ? (await utilitiesManager.evaluateFormulaValueAsync(
                      this.action.abilityCheck.formula,
                      this.actor.getRollData()
                  )) || 'd20'
                : 'd20';
            const autoAbilityCheckOnAttack = new ARSDice(this.actor, this.action, this.options).makeAbilityCheckRoll(
                this.action.abilityCheck.type,
                abilityCheckFormula,
                true
            );
        }

        if (isSpellSlot) {
            utilitiesManager.memslotSetUse(this.actor, slotInfo.slotType, slotInfo.slotLevel, slotInfo.slotIndex, true);
        }

        // no save or ability, at least throw up a "this was cast"
        if ((!forceSaveCheck && !forceAbilityCheck) || this?.action?.misc) {
            const speaker = ChatMessage.getSpeaker({ actor: this.actor });
            const isFriendly = this.actor.getToken().disposition === game.ars.const.TOKEN_DISPOSITIONS.FRIENDLY;
            ChatMessage.create({
                speaker: speaker,
                content:
                    `<div class="flexcol" > ` +
                    `<div class="${isFriendly ? '' : 'gm-only-view'}" style = "text-align: center;" > <h3>Cast <b>${
                        this.action.name
                    }</b></h3></div> ` +
                    (this?.action?.misc
                        ? `<div class="${isFriendly ? '' : 'gm-only-view'}" > <h4>${this.action.misc}</h4></div> `
                        : '') +
                    `<div class="non-owner-only-view" > ${game.i18n.localize('ARS.unknownAction')}</div> ` +
                    `</div> `,
                flags: {
                    world: {
                        context: {
                            action: this.action,
                            actorUuid: this.actor.uuid,
                            itemUuid: this.item.uuid,
                        },
                    },
                },
            });
        }
        //sound triggered using spell sound if configured
        if (this?.item?.system?.audio?.file) {
            this.item.playAudio();
        } else {
            this._playAudioCast(true, 'publicroll', false);
        }
    }
    async makeAbilityCheckRoll(abilityType, formula, automatedCheck = false) {
        async function _makeAbilityCheck(target) {
            const abilityRollCheck = new ARSRollAbilityCheck(
                this.token ? this.token : this.actor,
                abilityType,
                target,
                formula || 'd20',
                target ? target.actor.getRollData() : this.actor.getRollData(),
                {
                    event: this.options?.event,
                    skipSitiational: this.options?.event?.ctrlKey,
                }
            );
            const checkRolled = await abilityRollCheck.getMods(automatedCheck);
            if (!checkRolled) return;
            await abilityRollCheck.rollAbilityCheck();

            this._playAudioCheck(abilityRollCheck.success, abilityRollCheck.rollMode);

            this._sendCheckResultsMessage(abilityRollCheck);
            // console.log('dice.js makeAbilityCheckRoll', { abilityRollCheck });
        }

        if (!game.user.targets.size) {
            if (automatedCheck) {
                ui.notifications.error(`A target is required to force a ability check.`);
                return;
            }
            _makeAbilityCheck.bind(this)(undefined);
        } else {
            for (const target of game.user.targets) {
                _makeAbilityCheck.bind(this)(target);
            }
        }
        return this;
    }

    /**
     * Make a skill check roll and results
     *
     * @param {Boolean} skipSituational
     */
    async makeSkillRoll(skipSituational) {
        const skillCheck = new ARSRollSkill(
            this.token ? this.token : this.actor,
            this.skill,
            undefined,
            '',
            this.actor.getRollData(),
            {
                event: this.options?.event,
                skipSitiational: this.options?.event?.ctrlKey,
            }
        );
        const skillRolled = await skillCheck.getMods(skipSituational);
        if (!skillRolled) return;
        await skillCheck.rollSkill();

        this._playAudioCheck(skillCheck.success, skillCheck.rollMode);
        this._sendCheckResultsMessage(skillCheck);
        // console.log('dice.js makeSkillRoll', { skillCheck });
    }
    /**
     * Make a save check
     *
     * @param {Boolean} automatedSave, this is a save forced by spell casting/etc
     * @returns
     */
    async makeSaveRoll(automatedSave = false, saveType = undefined) {
        async function _makeSave(target) {
            const saveRollCheck = new ARSRollSave(
                this.token ? this.token : this.actor,
                saveType ? saveType : this.action,
                target,
                'd20',
                target ? target.actor.getRollData() : this.actor.getRollData(),
                {
                    event: this.options?.event,
                    skipSitiational: this.options?.event?.ctrlKey,
                }
            );
            const saveRolled = await saveRollCheck.getMods(automatedSave);
            if (!saveRolled) return;
            await saveRollCheck.rollSave();

            this._playAudioCheck(saveRollCheck.success, saveRollCheck.rollMode);

            if (target && automatedSave) {
                if (saveRollCheck.success) {
                    switch (this.action.successAction) {
                        // set save cache up for half damage
                        case 'halve':
                            utilitiesManager.setSaveCache(this.actor, target);
                            break;

                        // remove targets that saved
                        case 'remove':
                            game.user.targets.forEach((saved) => {
                                if (saved.id === target.id) {
                                    saved.setTarget(false, {
                                        user: game.user,
                                        releaseOthers: false,
                                        groupSelection: true,
                                    });
                                }
                            });
                            break;
                    }
                } else {
                    // this removed any cache that might be lingering
                    utilitiesManager.deleteSaveCache(target);

                    // if you fail a save you also get interrupted if casting
                    if (target.document.combatant && target.document.combatant.getFlag('world', 'initCasting')) {
                        utilitiesManager.chatMessage(
                            ChatMessage.getSpeaker({ actor: target.actor }),
                            'Casting Interrupted',
                            `${target.name} has casting interrupted`,
                            target.img
                        );
                    }
                }
            }

            this._sendCheckResultsMessage(saveRollCheck);
            // console.log('dice.js makeSaveRoll', { saveRollCheck });
        }

        if (!game.user.targets.size) {
            if (automatedSave) {
                ui.notifications.error(`A target is required to force a save check.`);
                return;
            }
            _makeSave.bind(this)(undefined);
        } else {
            for (const target of game.user.targets) {
                _makeSave.bind(this)(target);
            }
        }
        return this;
    }

    /**
     *
     * Send chat card with results of check (save/skill)
     *
     * @param {*} roll
     */
    async _sendCheckResultsMessage(roll) {
        const sourceSpeaker = ChatMessage.getSpeaker({ actor: this.actor });
        const checkTarget = roll.target ? roll.target : roll.actor;

        let checkAgainstName = roll.action?.name || '';
        // dont need these, the text is already in the check
        // if (!checkAgainstName && roll.abilityType) {
        //     checkAgainstName = game.i18n.localize(`ARS.abilityTypes.${roll.abilityType}`);
        // } else if (!checkAgainstName && this.saveType) {
        //     checkAgainstName = game.i18n.localize(`ARS.saveTypes.${this.saveType}`);
        // } else {
        //     ui.notifications.warn('dice.js _sendCheckResultsMessage: Unknown check name');
        // }

        let cardData = {
            isGM: game.user.isGM,
            speaker: sourceSpeaker,
            flavor: `<div data-id="${checkTarget.id}" class="secure-name">${checkTarget.name}</div><div>${roll.label}</div>${
                checkAgainstName ? `<div><b>${checkAgainstName}</b></div>` : ''
            }`,
            passedcheck: roll.success,
            targetnumber: roll.checkTarget,
            rolledcheck: roll.total,
            difference: roll.checkDiff,
            fumble: roll.fumble,
            critical: roll.critical,
            tooltip: await roll.getTooltip(),
            roll: roll,
            source: roll.actor,
            // data: data,
        };

        const content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-check.hbs', cardData);
        let chatData = {
            content: content,
            user: game.user.id,
            speaker: sourceSpeaker,
            roll: roll,
            rollMode: roll.rollMode,
            type: game.ars.const.CHAT_MESSAGE_TYPES.ROLL,
            flags: {
                world: {
                    context: {
                        action: this.action,
                        actorUuid: roll.actor.uuid,
                        itemUuid: this.actionSource?.uuid,
                    },
                },
            },
        };
        return ChatMessage.create(chatData);
    }
    /**
     *
     * Roll and apply damage
     *
     * @param {Boolean} isDamage
     * @returns
     */
    async makeDamageRoll(isDamage) {
        async function _makeDamage(target) {
            const damageDiceRoll = this.roll
                ? this.roll
                : new ARSRollDamage(
                      this.token ? this.token : this.actor,
                      this.weapon ? this.weapon : this.action,
                      target,
                      '',
                      this.actor.getRollData(),
                      {
                          event: this.options?.event,
                          skipSitiational: this.options?.event?.ctrlKey,
                      }
                  );
            if (!this.roll) {
                const rolledDamage = await damageDiceRoll.rollDamage();
                if (!rolledDamage) return;
                this.roll = damageDiceRoll;
            }
            // console.log('dice.js makeDamageRoll', { damageDiceRoll });

            const damagetype = damageDiceRoll.formulas[0].damageType;
            const arsdmg = new ARSDamage(
                isDamage,
                this.token ? this.token : this.actor,
                target,
                this.weapon ? this.weapon : this.action,
                damagetype,
                damageDiceRoll,
                this.options
            );
            await arsdmg.applyDamageAdjustments();
            // console.log('dice.js makeDamageRoll', { arsdmg });
            const diff = arsdmg.applyDamageToActor();
            // send damage chat message
            let weaponDmgFlavor = damageDiceRoll.damageStyle != 'normal' ? damageDiceRoll.damageStyle : '';
            arsdmg.sendDamageChatCard(diff, weaponDmgFlavor);

            // if we have other damage on weapon process with simple rolls
            if (damageDiceRoll.formulas.length > 1) {
                for (let i = 1; i < damageDiceRoll.formulas.length; i++) {
                    // console.log('dice.js makeDamageRoll OTHER', damageDiceRoll.formulas[i].formula);
                    const _rolledOtherDMG = this.dmgOther?.[i]
                        ? this.dmgOther[i]
                        : new ARSRoll(damageDiceRoll.formulas[i].formula, this.actor.getRollData(), this.options);
                    if (!this.dmgOther?.[i]) {
                        this.dmgOther[i] = _rolledOtherDMG;
                        await _rolledOtherDMG.roll();
                        if (game.dice3d) await game.dice3d.showForRoll(_rolledOtherDMG, game.user, true);
                    }

                    const otherDmgType = damageDiceRoll.formulas[i].damageType;
                    const dmgOther = new ARSDamage(
                        isDamage,
                        this.token ? this.token : this.actor,
                        target,
                        this.weapon ? this.weapon : this.action,
                        otherDmgType,
                        _rolledOtherDMG,
                        this.options
                    );
                    await dmgOther.applyDamageAdjustments();
                    // console.log('dice.js makeDamageRoll', { dmgOther, _rolledOtherDMG });
                    // send damage chat message
                    const otherDiff = dmgOther.applyDamageToActor();
                    dmgOther.sendDamageChatCard(otherDiff, 'Other');
                }
            }
        }

        if (!game.user.targets.size) {
            await _makeDamage.bind(this)(undefined);
        } else {
            for (const target of game.user.targets) {
                await _makeDamage.bind(this)(target);
            }
        }
        return this;
    }
    /**
     * wrapper function to fire off generic Action attack with parameters in right place
     */
    async makeActionAttack(slotInfo) {
        this.makeGenericAttack(
            slotInfo,
            this.action,
            utilitiesManager.useActionCharge,
            ARSRollAttack.prototype.rollActionAttack
        );
    }

    /**
     *
     * wrapper function to fire off generic Weapon attack with parameters in right place
     *
     */
    async makeWeaponAttack() {
        this.makeGenericAttack(undefined, this.weapon, utilitiesManager.useWeaponAmmo, ARSRollAttack.prototype.rollAttack);
    }

    /**
     * Make an attack by either weapon or action
     * @param {*} slotInfo spellslot info if any
     * @param {*} weaponOrAction weapon or action used
     * @param {*} useResourceFunction useActionCharge() or useWeaponAmmo()
     * @param {*} rollFunction rollAttack() rollActionAttack()
     */
    async makeGenericAttack(slotInfo, weaponOrAction, useResourceFunction, rollFunction) {
        const isSpellSlot = slotInfo != null;

        async function _makeAttack(target) {
            // console.log('dice.js makeGenericAttack', this.token, { weaponOrAction, target }, this);

            if (
                isSpellSlot &&
                utilitiesManager.isMemslotUsed(this.actor, slotInfo.slotType, slotInfo.slotLevel, slotInfo.slotIndex)
            ) {
                console.log('dice.js _makeAttack spell slot used already!');
                ui.notifications.warn(`${this.item.name} has no more uses left.`);
                if (!game.user.isGM) return;
            } else if (
                !isSpellSlot &&
                this.action &&
                game.ars.config.chargedActions.includes(this.action.type) &&
                this.action.resource.type != 'none'
            ) {
                if (!(await utilitiesManager.useActionCharge(this.actor, this.item, this.action))) {
                    const resourceUsed = game.i18n.localize(game.ars.config.consumed[this.action.resource.type] ?? 'uses');
                    console.log(`dice.js rollAttack ${this.action.name} has no more ${resourceUsed} left.`);
                    ui.notifications.warn(`${this.action.name} has no more ${resourceUsed} left.`);
                    if (!game.user.isGM) return;
                }
            }

            //Create dice roll object
            const attackRoll = new ARSRollAttack(
                this.token ? this.token : this.actor,
                weaponOrAction,
                target,
                'd20',
                this.actor.getRollData(),
                {
                    event: this.options?.event,
                    skipSitiational: this.options?.event?.ctrlKey,
                }
            );

            // get attack modifiers and situation input dialog values
            const attackRolled = await attackRoll.getAttackMods(this.roll ? true : false);
            if (!attackRolled) return;
            if (!this.roll) this.roll = attackRoll;

            // check has ammo/charges
            if (!(await useResourceFunction(this.actor, weaponOrAction, this.item))) {
                const resourceType = weaponOrAction.system?.attack?.type || weaponOrAction.resource?.type;
                const resourceUsed = game.i18n.localize(game.ars.config.consumed[resourceType] ?? 'uses');
                console.log(`dice.js makeGenericAttack ${weaponOrAction.name} has no more ${resourceUsed} left.`);
                ui.notifications.warn(`${weaponOrAction.name} has no more ${resourceUsed} left.`);
                if (!game.user.isGM) return;
            }

            if (isSpellSlot) {
                utilitiesManager.memslotSetUse(this.actor, slotInfo.slotType, slotInfo.slotLevel, slotInfo.slotIndex, true);
            }
            // roll dice!
            await rollFunction.call(attackRoll);

            // deal with stoneskin/mirror image/displacement
            const { label, hitSuccess } = await this._handleOnHitSpecials(target, attackRoll);

            // play audio/show message
            this._playAttackSounds(attackRoll);
            this._sendAttackMessage(attackRoll, label, hitSuccess);

            if (target) {
                // auto checks/saves/damage
                const autoDamage = game.settings.get('ars', 'autoDamage');
                const autoCheck = game.settings.get('ars', 'autoCheck');

                if (attackRoll.success && attackRoll.isWeapon && autoDamage) {
                    const autoWeaponDamage = await new ARSDice(this.token ? this.token : this.actor, this.weapon, {
                        event: this.options.event,
                    }).makeDamageRoll(true);
                } else if (attackRoll.success && autoDamage && (this.action.type === 'damage' || this.action.type === 'heal')) {
                    const autoActionDamage = await new ARSDice(this.token ? this.token : this.actor, this.action, {
                        event: this.options.event,
                    }).makeDamageRoll(this.action.type === 'damage');
                } else if (attackRoll.success && this.action && autoCheck && this.action.saveCheck.type !== 'none') {
                    const autoSaveOnAttack = new ARSDice(this.actor, this.action, this.options).makeSaveRoll(true);
                } else if (attackRoll.success && this.action && autoCheck && this.action.abilityCheck.type !== 'none') {
                    let abilityCheckFormula = this.action.abilityCheck.formula
                        ? (await utilitiesManager.evaluateFormulaValueAsync(
                              this.action.abilityCheck.formula,
                              this.actor.getRollData()
                          )) || 'd20'
                        : 'd20';
                    const autoAbilityCheckOnAttack = new ARSDice(this.actor, this.action, this.options).makeAbilityCheckRoll(
                        this.action.abilityCheck.type,
                        abilityCheckFormula,
                        true
                    );
                }
            }
        }

        if (!game.user.targets.size) {
            await _makeAttack.bind(this)(undefined);
        } else {
            for (const target of game.user.targets) {
                await _makeAttack.bind(this)(target);
            }
        }
    }

    /**
     * play success/failure sound with delay so dice have finished rolling
     * non-public rolls wont play sounds for players but will for GM
     *
     * @param {Boolean} success
     * @param {String} rollMode
     */
    _playAudioCheck(success = true, rollMode = 'publicroll') {
        const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
        const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
        // console.log('dice.js playAudioCheck', {
        //     success,
        //     rollMode,
        //     audioPlayTriggers,
        //     audioTriggersVolume,
        // });

        if (audioPlayTriggers) {
            const audioTriggerCheckFail = game.settings.get('ars', 'audioTriggerCheckFail');
            const audioTriggerCheckSuccess = game.settings.get('ars', 'audioTriggerCheckSuccess');

            if (['publicroll', 'selfroll', 'gmroll'].includes(rollMode)) {
                const _waitforit = setTimeout(
                    async () =>
                        AudioHelper.play(
                            {
                                src: success ? audioTriggerCheckSuccess : audioTriggerCheckFail,
                                volume: audioTriggersVolume,
                            },
                            rollMode === 'publicroll'
                        ),
                    game.dice3d ? 2500 : 0
                );
            }
        }
    }

    _playAudioCast(success = true, rollMode = 'publicroll', crit = false) {
        const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
        const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
        if (audioPlayTriggers) {
            const audioTriggerCast = game.settings.get('ars', 'audioTriggerCast');

            if (['publicroll', 'selfroll', 'gmroll'].includes(rollMode)) {
                AudioHelper.play({ src: audioTriggerCast, volume: audioTriggersVolume }, rollMode === 'publicroll');
            }
        }
    }

    /**
     * Check and play sounds for an attack with weapon
     *
     * @param {Object} roll
     */
    _playAttackSounds(roll) {
        if (this.weapon?.system?.audio?.success && this.weapon?.system?.audio?.failure) {
            this.weapon.playCheck(roll.rollMode, roll.success, roll.critical);
        } else {
            switch (roll.attackType) {
                case 'melee':
                    this._playAudioMelee(roll.success, roll.rollMode, roll.critical);
                    break;

                default:
                    this._playAudioMissile(roll.success, roll.rollMode, roll.critical);
                    break;
            }
        }
    }

    // add a slight delay to playing sound when
    // using DiceSoNice
    static PLAY_AUDIO_DELAY_3D = 1800;
    /**
     *
     * Play melee attack sound
     *
     * @param {*} success
     * @param {*} rollMode
     * @param {*} crit
     */
    _playAudioMelee(success = true, rollMode = 'publicroll', crit = false) {
        const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
        const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
        if (audioPlayTriggers) {
            const audioTriggerMeleeHit = game.settings.get('ars', 'audioTriggerMeleeHit');
            const audioTriggerMeleeCrit = game.settings.get('ars', 'audioTriggerMeleeCrit');
            const audioTriggerMeleeMiss = game.settings.get('ars', 'audioTriggerMeleeMiss');

            if (['publicroll', 'selfroll', 'gmroll'].includes(rollMode)) {
                const _waitforit = setTimeout(
                    async () =>
                        AudioHelper.play(
                            {
                                src: success ? (crit ? audioTriggerMeleeCrit : audioTriggerMeleeHit) : audioTriggerMeleeMiss,
                                volume: audioTriggersVolume,
                            },
                            rollMode === 'publicroll'
                        ),
                    game.dice3d ? this.constructor.PLAY_AUDIO_DELAY_3D : 0
                );
            }
        }
    }
    /**
     *
     * Play missile attack sound
     * @param {*} success
     * @param {*} rollMode
     * @param {*} crit
     */
    _playAudioMissile(success = true, rollMode = 'publicroll', crit = false) {
        const audioPlayTriggers = game.settings.get('ars', 'audioPlayTriggers');
        const audioTriggersVolume = game.settings.get('ars', 'audioTriggersVolume');
        // console.log('dice.js playAudioMissile', {
        //     success,
        //     rollMode,
        //     crit,
        //     audioPlayTriggers,
        //     audioTriggersVolume,
        // });
        if (audioPlayTriggers) {
            const audioTriggerRangeHit = game.settings.get('ars', 'audioTriggerRangeHit');
            const audioTriggerRangeMiss = game.settings.get('ars', 'audioTriggerRangeMiss');
            const audioTriggerRangeCrit = game.settings.get('ars', 'audioTriggerRangeCrit');

            if (['publicroll', 'selfroll', 'gmroll'].includes(rollMode)) {
                const _waitforit = setTimeout(
                    async () =>
                        AudioHelper.play(
                            {
                                src: success ? (crit ? audioTriggerRangeCrit : audioTriggerRangeHit) : audioTriggerRangeMiss,
                                volume: audioTriggersVolume,
                            },
                            rollMode === 'publicroll'
                        ),
                    game.dice3d ? this.constructor.PLAY_AUDIO_DELAY_3D : 0
                );
            }
        }
    }
    /**
     *
     * Send message for an attack.
     *
     * @param {*} roll
     * @param {*} label
     * @param {*} hitSuccess
     * @returns
     */
    async _sendAttackMessage(roll, label, hitSuccess) {
        // force miss if displaced/mirror/stoneskin even on 20
        const labelLower = label?.toLowerCase();
        const conditionsMiss = ['displaced', 'struck mirror', 'struck stoneskin'];
        const forceMissCondition = conditionsMiss.some((condition) => labelLower?.includes(condition));

        // check auto-hit/miss for dice
        if (game.ars.config.settings.autohitfail && roll.fumble) {
            roll.success = false;
        } else if (game.ars.config.settings.autohitfail && roll.critical && !forceMissCondition) {
            roll.success = true;
        }

        roll.success = hitSuccess;

        roll.diceToolTip = await roll.getTooltip();

        // Create a Chat Message
        const sourceSpeaker = ChatMessage.getSpeaker({ actor: this.actor });
        let cardData = {
            isGM: game.user.isGM,
            config: game.ars.config,
            speaker: sourceSpeaker,
            flavor: `${roll.label}${label}`,
            succeeded: roll.success,
            targetnumber: roll.targetAc,
            hitnumber: roll.acHit,
            rolledcheck: roll.total,
            difference: Math.abs(roll.targetAc - roll.acHit),
            fumble: roll.fumble,
            critical: roll.critical,
            tooltip: await roll.getTooltip(),
            roll: roll,
            source: this.actor,
            target: roll.target,
            weapon: this.weapon ? this.weapon : this.action,
            // action: dd.action,
        };

        const content = await renderTemplate('systems/ars/templates/chat/parts/chatCard-attack.hbs', cardData);
        let chatData = {
            content: content,
            user: game.user.id,
            speaker: sourceSpeaker,
            roll: roll,
            rollMode: roll.rollMode,
            type: game.ars.const.CHAT_MESSAGE_TYPES.ROLL,
            flags: {
                world: {
                    context: {
                        actorUuid: this.actor.uuid,
                        targetTokenUuid: roll.target?.document ? roll.target.document.uuid : undefined,
                        itemUuid: this.weapon?.uuid,
                        criticaled: roll.critical,
                        fumbled: roll.fumble,
                        // action: dd.data.sourceAction,
                    },
                },
            },
        };
        return ChatMessage.create(chatData);
    }

    /**
     *
     * Checks for specials like stoneskin/mirror/displacement that adjust
     * whether hit actually hit or not
     *
     * @param {*} target
     * @param {*} roll
     * @returns { label, hitSuccess }
     */
    async _handleOnHitSpecials(target, roll) {
        let label = '';
        let hitSuccess = roll.success;

        if (!target) return { label, hitSuccess };

        /**
         *
         * Reduce the change value for chindex by 1
         *
         * @param {*} effect
         * @param {*} chindex
         *
         */
        function effectChangeReductionHelper(effect, chindex) {
            const changeBundle = duplicate(effect.changes);
            changeBundle[chindex].value -= 1;
            effect.update({ changes: changeBundle });
        }

        /**
         * if has stoneskin and roll hits, remove 1 stoneskin and divert attack
         *
         * @param {*} effect
         * @param {*} change
         * @param {*} chindex
         * @param {*} depletedEffect
         *
         */
        function handleStoneskin(effect, change, chindex, depletedEffect) {
            if (hitSuccess) {
                const stoneCount = parseInt(change.value);
                if (stoneCount > 0) {
                    //STONESKIN HIT
                    hitSuccess = false;
                    label += ' STRUCK STONESKIN ';
                    if (change.value - 1 <= 0) depletedEffect.push(effect.id);
                    else effectChangeReductionHelper(effect, chindex);
                }
            }
        }

        /**
         *
         * If has mirror image and roll hits, check if it hit mirror, divert attack and remove one
         *
         * @param {*} effect
         * @param {*} change
         * @param {*} chindex
         * @param {*} depletedEffect
         */
        function handleMirrorImage(effect, change, chindex, depletedEffect) {
            if (hitSuccess) {
                const mirrorCount = parseInt(change.value);
                if (mirrorCount > 0) {
                    // check to see if hit mirror or target
                    const checkRoll = Math.floor(Math.random() * (mirrorCount + 1)) + 1;
                    // console.log("dice.js attack mirror image checkRoll!", checkRoll);
                    if (checkRoll == 1) {
                        //PC HIT
                    } else {
                        //MIRROR HIT
                        // remove a mirror
                        hitSuccess = false;
                        label += ' STRUCK MIRROR ';
                        if (change.value - 1 <= 0) depletedEffect.push(effect.id);
                        else effectChangeReductionHelper(effect, chindex);
                    }
                }
            }
        }
        /**
         *
         * if has displacement and first attack this combat, it's ignored
         *
         * @param {*} targetToken
         *
         */
        function handleDisplacement(targetToken) {
            if (targetToken?.combatant && !targetToken?.combatant?.getFlag('world', `displaced.${this.actor.uuid}`)) {
                targetToken?.combatant?.setFlag('world', `displaced.${this.actor.uuid}`, true);
                hitSuccess = false;
                label += ' DISPLACED ';
            }
            if (!targetToken?.combatant) {
                if (game.user.isGM)
                    ui.notifications.warn(
                        `${targetToken.name} has displacement but not in combat, GM must track occurance manually.`,
                        { permanent: true }
                    );
                console.warn(
                    `dice.js: handleDisplacement: ${targetToken.name} has displacement but not in combat, GM must track occurance manually.`
                );
            }
        }

        label += `<div data-id="${target.id}" class="secure-name">${target.name}</div> `;

        // Displacement/Mirrorimage/Stoneskin tests
        let depletedEffect = [];
        for (const [efindex, effect] of target.actor.getActiveEffects().entries()) {
            for (const [chindex, change] of effect.changes.entries()) {
                const changeKey = change.key.toLowerCase().trim();
                switch (changeKey) {
                    case 'special.displacement':
                        {
                            handleDisplacement.bind(this)(target);
                        }
                        break;
                    case 'special.mirrorimage':
                        {
                            handleMirrorImage.bind(this)(effect, change, chindex, depletedEffect);
                        }
                        break;
                    case 'special.stoneskin':
                        {
                            handleStoneskin.bind(this)(effect, change, chindex, depletedEffect);
                        }
                        break;
                }
            }
            // }
        } // end mirror/stone check
        if (depletedEffect.length)
            await utilitiesManager.runAsGM({
                operation: 'deleteActiveEffect',
                user: game.user.id,
                targetActorId: target.actor.id,
                targetTokenId: target.id,
                sourceActorId: this.actor.id,
                effectIds: depletedEffect,
            });
        return { label, hitSuccess };
    }
} // -------------------------------------------------------- END ARSDice
